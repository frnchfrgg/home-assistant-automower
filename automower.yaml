automower: # package
  script:
    arlette_send_key:
      mode: single
      sequence:
      - variables:
          keycode: |
            {{
              {
                "1": 1, "2": 2, "3": 3, "4": 4,
                "5": 5, "6": 6, "7": 7, "8": 8,
                "9": 9, "0": 0, "a": 10, "b": 11,
                "c": 12, "yes": 18, "cancel": 15,
              }.get((key|string).lower())
            }}
      - choose:
          - conditions: "{{ keycode != None }}"
            sequence:
              - service: mqtt.publish
                data:
                  topic: "Automower/Cmd/Keyboard"
                  payload_template: "{{ keycode }}"
    arlette_leave_station:
      mode: single
      sequence:
      - variables:
          chargetime: "{{ states('number.arlette_duree_max_charge_batterie')|default|int(200) }}"
      - service: number.set_value
        data:
          entity_id: number.arlette_duree_max_charge_batterie
          value: 0 # force mower to quit station
      - delay: 8 # wait until it left
      - service: number.set_value
        data:
          entity_id: number.arlette_duree_max_charge_batterie
          value: "{{ chargetime }}" # restore chargetime
  automation:
    - id: Parametragearlettemqtt
      alias: Paramétrage Automower MQTT
      description: ''
      mode: single
      trigger:
        - platform: homeassistant
          event: start
      condition: []
      action:
        - variables:
            nom: Arlette
            topic: Automower
            common:
              device:
                name: "{{nom}}"
                connections: [["mac", "48:3f:da:72:7b:9e"]]
                manufacturer: Husqvarna
                model: 220 AC
        - repeat:
            for_each:
              - type: lawn_mower
                unique_id: mower
                config:
                  name: "{{nom}}"
                  #icon: mdi:mower
                  activity_state_topic: "{{topic}}/State/Task"
                  activity_value_template: "{% raw %}
                    {{
                      {
                        0: 'error',
                        1: 'mowing',
                        2: 'mowing',
                        3: 'docked',
                        4: 'docked',
                      }.get(value_json, 'unknown')
                    }} {% endraw %}"
                  dock_command_topic: "{{topic}}/Cmd/Mode"
                  dock_command_template: "{% raw %}{{ 3 }}{% endraw %}"
                  start_mowing_command_topic: "{{topic}}/Cmd/Mode"
                  start_mowing_command_template: "{% raw %}{{ 1 }}{% endraw %}"
              - type: button
                unique_id: refresh
                config:
                  name: "{{nom}} Refresh"
                  command_topic: "{{topic}}/Action"
                  payload_press: "Refresh"
                  entity_category: config
              - type: button
                unique_id: update
                config:
                  name: "{{nom}} OTA Update"
                  command_topic: "{{topic}}/Action"
                  payload_press: "Update"
                  entity_category: config
              - type: button
                unique_id: fwsettings
                config:
                  name: "{{nom}} Firmware settings"
                  command_topic: "{{topic}}/Action"
                  payload_press: "Portal"
                  entity_category: config
              - type: button
                unique_id: trigger_alarm
                config:
                  name: "{{nom}} Trigger Alarm"
                  command_topic: "{{topic}}/Cmd/Alarmset"
                  payload_press: "1"
                  entity_category: config
              - type: select
                unique_id: state_mode
                config:
                  name: "{{nom}} Mode"
                  icon: mdi:mower
                  options:
                    - Parquer en station
                    - Cycles de tonte
                    - Tonte forcée
                  state_topic: "{{topic}}/State/Mode"
                  value_template: "
                    {{ '{{' }}
                      {
                        0: 'Tonte forcée',
                        1: 'Cycles de tonte',
                        3: 'Parquer en station',
                      }.get(value_json|int(-1), 'unknown')
                    {{ '}}' }}"
                  command_topic: "{{topic}}/Cmd/Mode"
                  command_template: "
                    {{ '{{' }}
                          {
                            'Tonte forcée': 0,
                            'Cycles de tonte': 1,
                            'Parquer en station': 3,
                          }.get(value|string)
                    {{ '}}' }}"
              - type: select
                unique_id: state_stop
                config:
                  name: "{{nom}} Clapet STOP"
                  options:
                    - Fermé
                    - Ouvert
                    - Ouverture forcée
                  state_topic: "{{topic}}/State/Stop"
                  value_template: "
                    {{ '{{' }} 'Ouverture forcée' if value_json > 55000 else
                        ('Ouvert' if value_json > 32000 else 'Fermé') {{ '}}' }}"
                  command_topic: "{{topic}}/Cmd/Stop"
                  command_template: "{{ '{{' }} 60000 if value == 'Ouverture forcée' else 0 {{ '}}' }}"
              - type: switch
                unique_id: setting_rotdir
                config:
                  name: "{{nom}} Rotation bidirectionnelle disque de coupe"
                  state_topic: "{{topic}}/State/Rotdir"
                  command_topic: "{{topic}}/Cmd/Rotdir"
                  payload_on: "1"
                  payload_off: "0"
                  icon: mdi:saw-blade
                  entity_category: config
              - type: switch
                unique_id: setting_minuterie
                config:
                  name: "{{nom}} Suspendre minuterie"
                  state_topic: "{{topic}}/State/Minuterie"
                  command_topic: "{{topic}}/Cmd/Minuterie"
                  payload_on: "1"
                  payload_off: "0"
                  icon: mdi:timer-off-outline
                  entity_category: config
              - type: number
                unique_id: setting_cbattmax
                config:
                  name: "{{nom}} Capacité batterie"
                  entity_category: config
                  state_topic: "{{topic}}/State/Cbattmax"
                  command_topic: "{{topic}}/Cmd/Cbattmax"
                  min: 1000
                  max: 5000
                  icon: mdi:battery
                  unit_of_measurement: mAH
              - type: number
                unique_id: setting_vbattemptya
                config:
                  name: "{{nom}} Tension retour Auto"
                  state_topic: "{{topic}}/State/VbattemptyA"
                  value_template: "{{ '{{' }} value_json|int(0) / 1000 {{ '}}' }}"
                  command_topic: "{{topic}}/Cmd/VbattemptyM" # bug
                  command_template: "{{ '{{' }} (value|float(0) * 1000)|int {{ '}}' }}"
                  min: 10
                  max: 25
                  step: 0.1
                  unit_of_measurement: V
                  icon: mdi:battery
                  entity_category: config
              - type: number
                unique_id: setting_vbattemptym
                config:
                  name: "{{nom}} Tension retour Manuel"
                  state_topic: "{{topic}}/State/VbattemptyM"
                  value_template: "{{ '{{' }} value_json|int(0) / 1000 {{ '}}' }}"
                  command_topic: "{{topic}}/Cmd/VbattemptyA" # bug
                  command_template: "{{ '{{' }} (value|float(0) * 1000)|int {{ '}}' }}"
                  min: 10
                  max: 25
                  step: 0.1
                  unit_of_measurement: V
                  icon: mdi:battery
                  entity_category: config
              - type: number
                unique_id: setting_chargetime
                config:
                  name: "{{nom}} Durée max charge batterie"
                  state_topic: "{{topic}}/State/Chargetime"
                  command_topic: "{{topic}}/Cmd/Chargetime"
                  unit_of_measurement: s
                  max: 3000
                  icon: mdi:battery-charging-outline
                  entity_category: config
              - type: number
                unique_id: setting_alarmtime
                config:
                  name: "{{nom}} Durée alarme"
                  state_topic: "{{topic}}/State/Alarmtime"
                  command_topic: "{{topic}}/Cmd/Alarmtime"
                  unit_of_measurement: s
                  entity_category: config
              - type: sensor
                unique_id: state_tbatt
                config:
                  name: "{{nom}} Température batterie"
                  state_topic: "{{topic}}/State/TBatt"
                  device_class: temperature
                  unit_of_measurement: °C
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_vbatt
                config:
                  name: "{{nom}} Tension batterie"
                  state_topic: "{{topic}}/State/VBatt"
                  device_class: voltage
                  unit_of_measurement: V
                  value_template: "{{ '{{' }} (value_json / 1000)|round(2) {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_icutpeak
                config:
                  name: "{{nom}} Temps entre pics de coupe"
                  state_topic: "{{topic}}/State/ICutpeak"
                  icon: mdi:summit
                  unit_of_measurement: s
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_icut
                config:
                  name: "{{nom}} Courant de coupe"
                  state_topic: "{{topic}}/State/ICut"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_icutmean
                config:
                  name: "{{nom}} Courant de coupe moyen"
                  state_topic: "{{topic}}/State/ICutmean"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_ileft
                config:
                  name: "{{nom}} Courant roue gauche"
                  state_topic: "{{topic}}/State/ILeft"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_ileftmean
                config:
                  name: "{{nom}} Courant moyen roue gauche"
                  state_topic: "{{topic}}/State/ILeftmean"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_iright
                config:
                  name: "{{nom}} Courant roue droite"
                  state_topic: "{{topic}}/State/IRight"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_irightmean
                config:
                  name: "{{nom}} Courant moyen roue droite"
                  state_topic: "{{topic}}/State/IRightmean"
                  device_class: current
                  unit_of_measurement: A
                  value_template: "{{ '{{' }} value_json / 100 {{ '}}' }}"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_sleft
                config:
                  name: "{{nom}} Vitesse roue gauche"
                  state_topic: "{{topic}}/State/SLeft"
                  icon: mdi:speedometer-medium
                  unit_of_measurement: cm/s
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_sright
                config:
                  name: "{{nom}} Vitesse roue droite"
                  state_topic: "{{topic}}/State/SRight"
                  icon: mdi:speedometer-medium
                  unit_of_measurement: cm/s
                  entity_category: diagnostic
              - type: sensor
                unique_id: error_fw
                config:
                  name: "{{nom}} État module"
                  state_topic: "{{topic}}/Error/FW"
                  entity_category: diagnostic
              - type: sensor
                unique_id: error_status
                config:
                  name: "{{nom}} État interface"
                  state_topic: "{{topic}}/Error/Status"
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_wifi
                config:
                  name: "{{nom}} Signal WiFi"
                  state_topic: "{{topic}}/State/Wifistrength"
                  unit_of_measurement: dBm
                  icon: mdi:wifi
                  entity_category: diagnostic
              - type: sensor
                unique_id: state_task
                config:
                  name: "{{nom}} Tâche en cours"
                  state_topic: "{{topic}}/State/Task"
                  icon: mdi:mower
                  value_template: "{% raw %}
                    {{
                      {
                        0: 'En panne',
                        1: 'Tonte',
                        2: 'Recherche station',
                        3: 'Charge',
                        4: 'Attente',
                      }.get(value_json, 'unknown')
                    }} {% endraw %}"
                  json_attributes_topic: "{{topic}}/State/Stats/Now"
              - type: sensor
                unique_id: state_stats_y
                config:
                  name: "{{nom}} Tâches d'hier"
                  state_topic: "{{topic}}/State/Stats/Y"
                  icon: mdi:mower
                  value_template: "{% raw %}
                    {%- for task in ('tonte', 'panne', 'recherche', 'charge', 'attente')
                                  if value_json[task] -%}
                      {%- if loop.first -%}{{task|title}}{%- endif -%}
                    {%- endfor -%}
                    {% endraw %}"
                  json_attributes_topic: "{{topic}}/State/Stats/Y"
              - type: sensor
                unique_id: state_status
                config:
                  name: "{{nom}} État"
                  state_topic: "{{topic}}/State/Status"
                  icon: mdi:mower
                  value_template: "{% raw %}
                    {{
                      {
                        0:    'Wifi perdu',
                        1:    'Wifi connecté',
                        6:    'Moteur de roue gauche bloqué',
                        8:    'Moteur de roue droit bloqué',
                        12:   'Pas de signal de boucle',
                        14:   'Coincée',
                        16:   'Hors zone de tonte',
                        18:   'Batterie faible',
                        24:   'Dérapée',
                        26:   'Station de charge inaccessible',
                        28:   'Nécessite charge manuelle',
                        30:   'Disque de coupe bloqué',
                        32:   'Disque de coupe freiné',
                        34:   'Tondeuse soulevée',
                        38:   'Tondeuse retournée',
                        42:   'Capteur boucle AR défectueux',
                        52:   'Pas de contact à la station de charge',
                        54:   'Pin expiré',
                        56:   'Capteur collision gauche défectueux',
                        58:   'Capteur collision droit défectueux',
                        1000: 'Démarrage',
                        1002: 'Tonte',
                        1006: 'Démarrage tondeuse',
                        1008: 'Démarrage tonte',
                        1012: 'Signal démarrage de tonte',
                        1014: 'En charge',
                        1016: 'Attente dans la station',
                        1024: 'Entrée dans la station',
                        1036: 'Tonte en carré',
                        1038: 'Bloquée',
                        1040: 'Collision',
                        1042: 'Recherche',
                        1044: 'Stop',
                        1046: 'Who knows 1046?',
                        1048: 'Arrimée',
                        1050: 'Sortie de la station ',
                        1052: 'Pressez YES pour démarrer',
                        1056: 'En attente (Mode Manuel/Home)',
                        1058: 'Suivi du cable guide',
                        1060: 'N-Signal trouvé',
                        1062: 'Zone éloignée inaccessible',
                        1064: 'Recherche',
                        1066: 'Problème de suivi zone éloignée',
                        1070: 'Suivi du guide 1',
                        1072: 'Suivi du guide 2',
                      }.get(value_json, 'unknown: ' + value_json|string)
                    }} {% endraw %}"
            sequence:
              - variables:
                  id: "{{nom.lower()}}_{{repeat.item.unique_id}}"
              - service: mqtt.publish
                data:
                  topic: |
                    homeassistant/{{repeat.item.type}}/{{id}}/config
                  payload: |
                    {{ dict(
                      common.items()|list
                      + repeat.item.config.items()|list
                      + [("unique_id", id)]
                    ) | to_json }}
    - id: '1620733068758'
      alias: Arlette Time sync
      trigger:
      - platform: mqtt
        topic: Automower/Timereq
        payload: '1'
      condition: []
      action:
      - service: mqtt.publish
        data:
          topic: Automower/Time
          payload_template: "{{ now().timestamp()|round(3) }}"
      mode: single
    - id: '1622061632706'
      alias: Arlette Arrêt tonte la nuit
      description: ''
      trigger:
      - platform: template
        value_template: '{{ state_attr("sun.sun", "elevation")|float(0) < (0 if is_state("sensor.arlette_tache_en_cours",
          "Charge") else -12) }}'
      condition: []
      action:
      - service: select.select_option
        data:
          option: Parquer en station
        target:
          entity_id: select.arlette_mode
      mode: single
    - id: '1647625303931'
      alias: Arlette Notification panne
      description: ''
      trigger:
      - platform: state
        entity_id: sensor.arlette_tache_en_cours
        to: En panne
        for:
          hours: 0
          minutes: 5
          seconds: 0
      condition: []
      action:
      - service: notify.all_phones
        data:
          message: 'Arlette est en panne: {{ states("sensor.arlette_etat") }}'
          title: Arlette est en panne
      mode: single
